/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.hash.multilogweb;

import java.io.IOException;
import java.util.HashMap;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 *
 * @author draygoza
 */
public class Handlers {
    static private HashMap<String, Handler> logs = null;
    static private Logger LOGGER = Logger.getLogger("mx.hash.multilogweb.Handlers");
    
    static public HashMap<String, Handler> obtenerLogs() throws IOException {
        
        if(Handlers.logs == null) {
            LOGGER.log(Level.INFO, "Creando handlers");
            
            SimpleFormatter simpleFormatter = new SimpleFormatter();
            Handler log0 = new FileHandler("C:\\Users\\draygoza\\Desktop\\MultiLogs\\bitacora_terminal_principal.log");
            Handler log1 = new FileHandler("C:\\Users\\draygoza\\Desktop\\MultiLogs\\bitacora_terminal_1.log");
            Handler log2 = new FileHandler("C:\\Users\\draygoza\\Desktop\\MultiLogs\\bitacora_terminal_2.log");
            Handler log3 = new FileHandler("C:\\Users\\draygoza\\Desktop\\MultiLogs\\bitacora_terminal_3.log");
            Handler log4 = new FileHandler("C:\\Users\\draygoza\\Desktop\\MultiLogs\\bitacora_terminal_4.log");
            
            log0.setFormatter(simpleFormatter);
            log1.setFormatter(simpleFormatter);
            log2.setFormatter(simpleFormatter);
            log3.setFormatter(simpleFormatter);
            log4.setFormatter(simpleFormatter);
            
            Handlers.logs = new HashMap<>();
            Handlers.logs.put("0", log0);
            Handlers.logs.put("1", log1);
            Handlers.logs.put("2", log2);
            Handlers.logs.put("3", log3);
            Handlers.logs.put("4", log4);
        }
        
        return Handlers.logs;
    }
    
    static public void cerrarLogs() {
        if(Handlers.logs != null) {
            Handler h = Handlers.logs.get("0");
            h.close();
            
            h = Handlers.logs.get("1");
            h.close();
            
            h = Handlers.logs.get("2");
            h.close();
            
            h = Handlers.logs.get("3");
            h.close();
            
            h = Handlers.logs.get("4");
            h.close();
        }
    }
        
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.hash.multilogweb;

import java.io.IOException;
import java.util.HashMap;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author draygoza
 */
@WebServlet(name = "LogServlet", urlPatterns = {"/multi/LogServlet"})
public class LogServlet extends HttpServlet {
    static private final Logger LOGGER  = Logger.getLogger("mx.hash.multilogweb.LogServlet");
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response){
        try {
            String tpos = request.getParameter("posicion");
            Handler log = new FileHandler("C:\\Users\\draygoza\\Desktop\\MultiLogs\\bitacora_terminal_" + tpos + ".log", true);
            
            log.setFormatter( new SimpleFormatter() );
            LOGGER.addHandler( log );
            LOGGER.log(Level.INFO, "Operacion en terminal {0}", tpos);
            LOGGER.removeHandler( log );
            log.close();
            
            response.setStatus(200);
            response.getWriter().write("Operacion en " + tpos);
            
        } catch (IOException ex) {
            Logger.getLogger(LogServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
    
}
